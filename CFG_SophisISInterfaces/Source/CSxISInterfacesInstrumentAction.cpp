#pragma warning(disable:4251)
/*
** Includes
*/
#include <stdio.h>
#include "SphTools/SphLoggerUtil.h"
#include __STL_INCLUDE_PATH(string)
#include "SphLLInc/misc/ConfigurationFileWrapper.h"
#include "SphInc/instrument/SphInstrument.h"
#include "SphSDBCInc/SphSQLQuery.h"

// specific
#include "CSxISInterfacesInstrumentAction.h"


using namespace sophis::misc;
using namespace sophis::sql;

/*
** Methods
*/
//-------------------------------------------------------------------------------------------------------------
CONSTRUCTOR_INSTRUMENT_ACTION(CSxISInterfacesInstrumentAction)

//-------------------------------------------------------------------------------------------------------------
/*virtual*/ void CSxISInterfacesInstrumentAction::VoteForCreation(CSRInstrument &instrument)
throw (VoteException , ExceptionBase)
{	
	UpdateISInterfacesExternalReferences(instrument,"1","KO");
}

//-------------------------------------------------------------------------------------------------------------
/*virtual*/ void CSxISInterfacesInstrumentAction::VoteForModification(CSRInstrument &instrument, NSREnums::eParameterModificationType type)
throw (VoteException, ExceptionBase)
{
	char externalRefVal[100];
	
	GetCFGExternalReference(instrument,externalRefVal);
	
	if (strcmp(externalRefVal,"") != 0)
	{
		UpdateISInterfacesExternalReferences(instrument,"2","KO");
	}
	else
	{
		UpdateISInterfacesExternalReferences(instrument,"1","KO");
	}	
}

void CSxISInterfacesInstrumentAction::GetCFGExternalReference(CSRInstrument &instrument,char* val)
{
	strcpy_s(val,100,"");
	
	_STL::string CFGExternalRefName = "";	

	ConfigurationFileWrapper::getEntryValue("CFG_SOPHIS_IS_INTERFACES", "CFGExternalRefName",CFGExternalRefName,"CFGExternalRef");	

	char SQLQuery[1024];
	sprintf_s(SQLQuery, 1024,"select value from extrnl_references_instruments I, extrnl_references_definition D where I.REF_IDENT = D.REF_IDENT and" 
		" D.ref_name = '%s' and I.sophis_ident = %ld", CFGExternalRefName.c_str(),instrument.GetCode());

	struct SSxResults
	{
		char fRefValue[100];
	};

	SSxResults *resultBuffer = NULL;
	int		 nbResults = 0;

	CSRStructureDescriptor	desc(1, sizeof(SSxResults));

	ADD(&desc, SSxResults, fRefValue, rdfString);

	CSRSqlQuery::QueryWithNResults(SQLQuery,&desc,(void **) &resultBuffer,&nbResults);

	if (nbResults > 0)
	{
		strcpy_s(val,100,resultBuffer[0].fRefValue);
	}

	free((char*)resultBuffer);
}

void CSxISInterfacesInstrumentAction::UpdateISInterfacesExternalReferences(CSRInstrument &instrument, const char* actionTypeValue, const char* integrStatusValue)
{
	_STL::string actionTypeExternalRefName = "";
	_STL::string integrStatusExternalRefName = "";

	ConfigurationFileWrapper::getEntryValue("CFG_SOPHIS_IS_INTERFACES", "ActionTypeExternalRefName",actionTypeExternalRefName,"CFGActionType");
	ConfigurationFileWrapper::getEntryValue("CFG_SOPHIS_IS_INTERFACES", "IntegrStatusExternalRefName",integrStatusExternalRefName,"CFGIntegrStatus");

	SSComplexReferenceP initReferences;

	initReferences = instrument.GetClientReference();

	bool foundActionTypeRef = false;
	bool foundIntegrStatusRef = false;

	for(int i = 0; i < initReferences.refNum; i++)
	{
		if( strcmp(initReferences.ref[i].type,actionTypeExternalRefName.c_str()) == 0 )
		{
			strcpy_s(initReferences.ref[i].value,100,actionTypeValue);
			foundActionTypeRef = true;			
		}

		if( strcmp(initReferences.ref[i].type,integrStatusExternalRefName.c_str()) == 0 )
		{
			strcpy_s(initReferences.ref[i].value,100,integrStatusValue);
			foundIntegrStatusRef = true;			
		}

		if (foundActionTypeRef == true && foundIntegrStatusRef == true)
			break;
	}

	SSComplexReferenceP newReferences;
	if(foundActionTypeRef == false && foundIntegrStatusRef == false)
	{
		newReferences.refNum = initReferences.refNum + 2;
	}
	else if (foundActionTypeRef == true && foundIntegrStatusRef == true)
	{
		newReferences.refNum = initReferences.refNum;
	}
	else
	{
		newReferences.refNum = initReferences.refNum + 1;
	}

	newReferences.ref = new SSComplexReference[newReferences.refNum];
	for(int i = 0; i < initReferences.refNum; i++)
	{
		strcpy_s(newReferences.ref[i].type,100, initReferences.ref[i].type);
		strcpy_s(newReferences.ref[i].value,100, initReferences.ref[i].value);
	}

	int lastIndex = initReferences.refNum;

	if(foundActionTypeRef == false)
	{
		SSComplexReference CFGActionTypeRef;
		strcpy_s(CFGActionTypeRef.type,100,actionTypeExternalRefName.c_str());
		strcpy_s(CFGActionTypeRef.value,100, actionTypeValue);
		newReferences.ref[lastIndex] = CFGActionTypeRef;
		lastIndex++;
	}

	if(foundIntegrStatusRef == false)
	{
		SSComplexReference CFGIntegrStatusRef;
		strcpy_s(CFGIntegrStatusRef.type,100,integrStatusExternalRefName.c_str());
		strcpy_s(CFGIntegrStatusRef.value,100,integrStatusValue);
		newReferences.ref[lastIndex] = CFGIntegrStatusRef;
	}

	instrument.SetClientReference(newReferences);
}
