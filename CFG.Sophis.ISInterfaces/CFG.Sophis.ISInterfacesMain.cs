using System;
using sophisTools;
using sophis;

namespace CFG.Sophis.ISInterfaces_Console
{
    /// <summary>
	/// </summary>
    class API_Launcher
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			sophis.AssemblyLoadResolver.Instance.InstallResolver();
			Init(args);
		}
	
	    static void Init(string[] args)
		{
			Console.WriteLine("API Console Application");

			CSMApi api = new CSMApi();

			try
			{
				// Initialize the API
				api.Initialise();

				// to do
			}
		    catch(Exception ex)
			{
				Console.WriteLine(ex.Message+" (press a key)");
				Console.Read();
			}
			finally
			{
				api.DisposeApi();
                Console.WriteLine("API Console Application closed (press a key)");
			}
			Console.Read();
		}
		
	}
}
