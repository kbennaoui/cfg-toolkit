#ifndef __CFG_YieldCurveLinebyLine_H__
#define __CFG_YieldCurveLinebyLine_H__

#include "SphInc/market_data/SphYieldCurve.h"
#include "SphInc/SphEnums.h"
#include "SphTools/SphExceptions.h"

class CFG_YieldCurveLinebyLine : public sophis::market_data::CSRYieldCurve
{
//------------------------------------ PUBLIC ---------------------------------
public:
	DECLARATION_YIELD_CURVE(CFG_YieldCurveLinebyLine);
	virtual void Initialize(const SSYieldCurve& curve, bool computeZeroCouponYieldCurve, bool validationYieldCurve);
	virtual void InitialiseConstructor(const SSYieldCurve& curve, bool computeZeroCouponYieldCurve, bool validationYieldCurve);
	virtual void InitialiseConstructorWithSpreadMgr(const SSYieldCurve& curve, bool computeZeroCouponYieldCurve, bool validationYieldCurve);

	virtual double GetInterpolatedZeroCouponRate( double ratePrevious, double rateNext, 
		double timeToMaturityPrevious, double timeToMaturityNext, double timeToMaturity, const CSRCalendar* calendar) const;
	virtual double CompoundFactor(double timeToMaturity, double overRate=0) const;

	mutable int fShift;
};

#endif //!CFG_YieldCurveLinebyLine_H__