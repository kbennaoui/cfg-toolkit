#ifndef _CFG_FixedIncome_Version_H_
#define _CFG_FixedIncome_Version_H_

#include "SphInc/SphBuildVersion.h"

#define CFG_FixedIncome_VERSION 			2, 0, 0, 8
#define CFG_FixedIncome_VERSION_STR			"2.0.0.8\0"
#ifdef WIN64
	#define CFG_FixedIncome_TOOLKIT_DESCRIPTION	"CFG_MoroccanFixedIncome (x64)"
#else
	#define CFG_FixedIncome_TOOLKIT_DESCRIPTION	"CFG_MoroccanFixedIncome (x86)"
#endif

#endif // _CFG_FixedIncome_Version_H_
