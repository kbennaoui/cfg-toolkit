#ifndef __CSxConstantAmortizedBond_H__
#define __CSxConstantAmortizedBond_H__

/*
** Includes
*/
#include "SphInc/instrument/SphInstrument.h"
#include "SphInc/instrument/SphBond.h"
#include "SphInc/market_data/SphMarketData.h"

/*
** Class definition for the bond model.
*/
class CFG_ConstantAmortizedBond : public sophis::instrument::CSRBond
{
//------------------------------------ PUBLIC ---------------------------------
public:
	DECLARATION_BOND(CFG_ConstantAmortizedBond);
	
	virtual double	ComputeNonFlatCurvePrice(	long	transactionDate,
												long	settlementDate,		
												long	ownershipDate,
												const market_data::CSRMarketData		&param,
												double									*derivee,
												const static_data::CSRDayCountBasis		*base,
												short									adjustedDates,
												short									valueDates,
												const static_data::CSRDayCountBasis		*dayCountBasis,
												const static_data::CSRYieldCalculation	*yieldCalculation,
												_STL::vector<SSRedemption>				*redemptionArray,
												_STL::vector<SSBondExplication>			*explicationArray,
												bool									withSpreadMgt,
												bool									throwException) const;
	
	virtual double	GetDirtyPriceByYTM(			
		long 						transactionDate, 
		long 						settlementDate,
		long						pariPassuDate,
		double 						yieldToMaturity,
		short						adjustedDates,
		short						valueDates=kUseDefaultValue,
		const CSRDayCountBasis		*dayCountBasis = 0, 
		const CSRYieldCalculation	*yieldCalculation = 0,
		const CSRMarketData			&context = *gApplicationContext) const;

	virtual double	GetYTMByDirtyPrice(			
		long 						transactionDate, 
		long 						settlementDate,
		long						pariPassuDate,
		double 						dirtyPrice,
		short						adjustedDates,
		short						valueDates=kUseDefaultValue,
		const CSRDayCountBasis		*dayCountBasis = 0, 
		const CSRYieldCalculation	*yieldCalculation = 0,
		const CSRMarketData			&context = *gApplicationContext) const;

	virtual void	InitDefaultValue(	short						&adjustedDatesCalc,
										short						&valueDatesCalc,
										const static_data::CSRDayCountBasis		**dayCountBasis, 
										const static_data::CSRYieldCalculation	**yieldCalculation) const;

	virtual SSAlert* NewAlertList(long forecastDate, int* nb) const;
	
	virtual bool IsRevisable() const;
	virtual long GetRevisedExpiry(long transactionDate) const;
	virtual CSRBond* GetRevisedClone(long transactionDate) const;
};

#endif // __CSxConstantAmortizedBond_H__
