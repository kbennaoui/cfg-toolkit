//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CFG_FixedIncome.rc
//
#define IDC_RISK_SPREAD                 601
#define IDD_BOND_DIALOG                 6500
#define IDD_BOND_CALCULATION_TAB        6560

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        6002
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         602
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
