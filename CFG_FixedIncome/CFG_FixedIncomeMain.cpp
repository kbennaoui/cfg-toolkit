///{{SOPHIS_TOOLKIT_INCLUDE (do not delete this line)
#include "SphInc/SphMacros.h"
#include "SphTools/SphCommon.h"
#include "SphInc/SphPreference.h"
#include "version/CFG_FixedIncomeVersion.h"
#include "CFG_YieldCurveLinebyLine.h"
#include "CFG_YieldCurveFixedPoints.h"
#include "CFG_YieldCurveZCMAD.h"
#include "CFG_GlobalFunctions.h"
#include "CFG_BondDialog.h"
#include "CFG_StaticSpreadBond.h"
#include "CFG_ConstantAmortizedBond.h"
#include "CFG_RevisionClause.h"
#include "CFG_RevisableBondAction.h"
#include "CFG_DayCountBasis.h"
//}}SOPHIS_TOOLKIT_INCLUDE

UNIVERSAL_MAIN
{
	InitLinks(indirectionPtr, TOOLKIT_VERSION); // mandatory
	CSRPreference::SetToolkitVersion(CFG_FixedIncome_TOOLKIT_DESCRIPTION);

//{{SOPHIS_INITIALIZATION (do not delete this line)
	INITIALISE_YIELD_CURVE(CFG_YieldCurveLinebyLine, "Ligne a Ligne MAD");
	INITIALISE_YIELD_CURVE(CFG_YieldCurveFixedPoints, "Points Fixes MAD");
	INITIALISE_YIELD_CURVE(CFG_YieldCurveZCMAD, "ZC MAD");
	INITIALISE_GLOBAL_FUNCTIONS(CFG_GlobalFunctions);
	INITIALISE_STANDARD_DIALOG(CFG_BondDialog, kBondDialogId);
	INITIALISE_BOND(CFG_StaticSpreadBond, "Titre MAD remboursable in fine");
	INITIALISE_BOND(CFG_ConstantAmortizedBond, "Titre MAD amortissable");
	INITIALISE_CLAUSE(CFG_RevisionClause, CSRClause, "Revision");
	INITIALISE_INSTRUMENT_ACTION(CFG_RevisableBondAction, oUser1, "Revisable Bond Action");
	INITIALISE_DAY_COUNT_BASIS(CFG_ActActCouponDayCountBasis, "Act/Act CFG Coupon");
//}}SOPHIS_INITIALIZATION

};
